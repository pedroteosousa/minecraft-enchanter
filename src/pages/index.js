import React, { useEffect, useState } from 'react'

import ItemPicker from '../components/ItemPicker'
import ItemCard from '../components/ItemCard'

import { COST, MAX_ITEMS, Item, solve } from '../enchanter'

const COLORS = [
  '#ff9aa2',
  '#ffb7b2',
  '#ffdac1',
  '#e2f0cb',
  '#b5ead7',
]

const App = () => {
  const [editing, setEditing] = useState([undefined, undefined])

  const [items, setItems] = useState([Item('Boots')])

  const [solution, setSolution] = useState([])
  const [colors, setColors] = useState({})


  useEffect(() => {
    const allItems = []
    items.slice(1).forEach(item => {
      allItems.push(item)
    })
    solution.forEach(([p1, p2, result]) => {
      allItems.push(p1)
      allItems.push(p2)
      allItems.push(result)
    })

    const allItemHashes = []
    const newColors = {}
    allItems.forEach(item => {
      allItemHashes.push([item.hash(), item.hash(0)])
    })
    allItems.forEach(item => {
      if (item.penalty === 0) return
      allItemHashes.forEach(([hash, hashp]) => {
        if (hashp === item.hash(0) && hash !== item.hash()) {
          if (!(item.hash() in newColors)) {
            newColors[item.hash()] = COLORS[item.penalty]
          }
        }
      })
    })
    setColors(newColors)
  }, [solution, items])

  return (
    <>
      <div className="container">
        <div className="inputs">
          <div className="target">
            <p className="card-list-title">Target Item</p>
            <ItemCard item={items[0]} onEdit={() => setEditing([0, items[0]])}/>
          </div>
          <div className="sacrifices">
            <p className="card-list-title">Sacrifices</p>
            <div className="card-list">
              {
                items.slice(1).map((item, index) => (
                  <ItemCard
                    item={item}
                    color={(item.hash() in colors) ? colors[item.hash()] : undefined}
                    onEdit={() => setEditing([index + 1, item])}
                    onRemove={() => {
                      const cp = [...items]
                      cp.splice(index + 1, 1)
                      setItems(cp)
                    }}
                    key={index}
                  />
                ))
              }
            </div>
          </div>
          {
            items.length <= MAX_ITEMS
            ? <div className="add-item-button" onClick={() => {
                var newItems = [...items, Item('Book')]
                setItems(newItems)
              }}>Add Item</div>
            : null
          }
        </div>
        <div className="solution">
          <div className="solution-buttons">
            <div className="add-item-button" onClick={() => {
              const s = solve(items[0], items.slice(1), COST.XP)
              s.reverse()
              if (s.length === 0) {
                alert("No solution found")
              }
              setSolution(s)
            }}>Minimize Total Experience</div>
            <div className="add-item-button" onClick={() => {
              const s = solve(items[0], items.slice(1), COST.Levels)
              s.reverse()
              if (s.length === 0) {
                alert("No solution found")
              }
              setSolution(s)
            }}>Minimize Total Levels</div>
          </div>
          {
            solution.map(([p1, p2, target]) => {
              return (
                <div className="solution-box">
                  <ItemCard item={p1} color={(p1.hash() in colors) ? colors[p1.hash()] : undefined} />
                  <p className="solution-box-text">+</p>
                  <ItemCard item={p2} color={(p2.hash() in colors) ? colors[p2.hash()] : undefined} />
                  <p className="solution-box-text">=</p>
                  <ItemCard item={target} color={(target.hash() in colors) ? colors[target.hash()] : undefined} />
                  <p className="solution-cost">Cost: {target.cost} level(s)</p>
                </div>
              )
            })
          }
        </div>
      </div>
      <ItemPicker
        item={editing[1]}
        targetType={editing[0] === 0 ? undefined : items[0].type}
        showAnvilUsesPicker={editing[0] !== 0}
        onDone={editedItem => {
          if (editing[0] === 0 && editedItem.type !== items[0].type) {
            if (window.confirm("Changing target type will cause all sacrifice items and current solution to disappear")) {
              setItems([editedItem])
              setEditing([undefined, undefined])
              setSolution([])
            }
            return
          }
          var newItems = [...items]
          newItems[editing[0]] = editedItem
          setItems(newItems)
          setEditing([undefined, undefined])
        }}
        isOpen={editing[0] !== undefined}
      />
    </>
  )
}

export default App
