const MAX_ITEMS = 10
const MAX_PENALTY = 5

const ROMAN_NUMERALS = ['I', 'II', 'III', 'IV', 'V']

const COST = {
  XP: 'XP',
  Levels: 'Levels',
}

const TYPES = {
  Book: 'Book',
  Helmet: 'Helmet',
  Chestplate: 'Chestplate',
  Leggings: 'Leggings',
  Boots: 'Boots',
  Sword: 'Sword',
  Axe: 'Axe',
  Pickaxe: 'Pickaxe',
  Shovel: 'Shovel',
  Hoe: 'Hoe',
  Shears: 'Shears',
  Bow: 'Bow',
  Crossbow: 'Crossbow',
  FishingRod: 'Fishing Rod',
  FlintAndSteel: 'Flint and Steel',
  Trident: 'Trident',
  Shield: 'Shield',
  Elytra: 'Elytra',
}

const Enchantment = (level, multiplier = 0, compatible_items = [], incompatibilities = []) => {
  const enchantment = {
    level,
    multiplier,
    compatible_items,
    incompatibilities,
  }
  enchantment.get_multiplier = (type) => {
    if (type === TYPES.Book) return enchantment.multiplier === 1 ? 1 : enchantment.multiplier / 2;
    return enchantment.multiplier;
  }
  enchantment.hash = (name)  => {
    return `${name};${enchantment.level}`
  }
  enchantment.can_apply = item => {
    if (item.type !== TYPES.Book && !enchantment.compatible_items.includes(item.type)) {
      return [false, 0, item.type]
    }
    var isCompatible = true
    var reason = ''
    Object.keys(item.enchantments).forEach(name => {
      if (enchantment.incompatibilities.includes(name)) {
        isCompatible = false
        reason = name
      }
    })
    return [isCompatible, isCompatible ? 0 : 1, reason]
  }
  return enchantment
}

const ENCHANTMENTS = {
  "Protection":            Enchantment(4, 1, [TYPES.Helmet, TYPES.Chestplate, TYPES.Leggings, TYPES.Boots], ['Fire Protection', 'Blast Protection', 'Projectile Protection']),
  "Fire Protection":       Enchantment(4, 2, [TYPES.Helmet, TYPES.Chestplate, TYPES.Leggings, TYPES.Boots], ['Protection', 'Blast Protection', 'Projectile Protection']),
  "Feather Falling":       Enchantment(4, 2, [TYPES.Boots]),
  "Blast Protection":      Enchantment(4, 4, [TYPES.Helmet, TYPES.Chestplate, TYPES.Leggings, TYPES.Boots], ['Protection', 'Fire Protection', 'Projectile Protection']),
  "Projectile Protection": Enchantment(4, 2, [TYPES.Helmet, TYPES.Chestplate, TYPES.Leggings, TYPES.Boots], ['Protection', 'Fire Protection', 'Blast Protection']),
  "Thorns":                Enchantment(3, 8, [TYPES.Helmet, TYPES.Chestplate, TYPES.Leggings, TYPES.Boots]),
  "Respiration":           Enchantment(3, 4, [TYPES.Helmet]),
  "Depth Strider":         Enchantment(3, 4, [TYPES.Boots], ['Frost Walker']),
  "Aqua Affinity":         Enchantment(1, 4, [TYPES.Helmet]),
  "Sharpness":             Enchantment(5, 1, [TYPES.Sword, TYPES.Axe], ['Smite', 'Bane of Arthropods']),
  "Smite":                 Enchantment(5, 2, [TYPES.Sword, TYPES.Axe], ['Sharpness', 'Bane of Arthropods']),
  "Bane of Arthropods":    Enchantment(5, 2, [TYPES.Sword, TYPES.Axe], ['Sharpness', 'Smite']),
  "Knockback":             Enchantment(2, 2, [TYPES.Sword]),
  "Fire Aspect":           Enchantment(2, 4, [TYPES.Sword]),
  "Looting":               Enchantment(3, 4, [TYPES.Sword]),
  "Efficiency":            Enchantment(5, 1, [TYPES.Axe, TYPES.Pickaxe, TYPES.Shovel, TYPES.Hoe, TYPES.Shears]),
  "Silk Touch":            Enchantment(1, 8, [TYPES.Axe, TYPES.Pickaxe, TYPES.Shovel, TYPES.Hoe], ['Fortune']),
  "Unbreaking":            Enchantment(3, 2, [TYPES.Helmet, TYPES.Chestplate, TYPES.Leggings, TYPES.Boots, TYPES.Sword, TYPES.Axe, TYPES.Pickaxe, TYPES.Shovel, TYPES.Hoe, TYPES.Shears, TYPES.Bow, TYPES.Crossbow, TYPES.FishingRod, TYPES.FlintAndSteel, TYPES.Trident, TYPES.Shield, TYPES.Elytra]),
  "Fortune":               Enchantment(3, 4, [TYPES.Axe, TYPES.Pickaxe, TYPES.Shovel, TYPES.Hoe], ['Silk Touch']),
  "Power":                 Enchantment(5, 1, [TYPES.Bow]),
  "Punch":                 Enchantment(2, 4, [TYPES.Bow]),
  "Flame":                 Enchantment(1, 4, [TYPES.Bow]),
  "Infinity":              Enchantment(1, 8, [TYPES.Bow], ['Mending']),
  "Luck of the Sea":       Enchantment(3, 4, [TYPES.FishingRod]),
  "Lure":                  Enchantment(3, 4, [TYPES.FishingRod]),
  "Frost Walker":          Enchantment(2, 4, [TYPES.Boots], ['Depth Strider']),
  "Mending":               Enchantment(1, 4, [TYPES.Helmet, TYPES.Chestplate, TYPES.Leggings, TYPES.Boots, TYPES.Sword, TYPES.Axe, TYPES.Pickaxe, TYPES.Shovel, TYPES.Hoe, TYPES.Shears, TYPES.Bow, TYPES.Crossbow, TYPES.FishingRod, TYPES.FlintAndSteel, TYPES.Trident, TYPES.Shield, TYPES.Elytra], ['Infinity']),
  "Curse of Binding":      Enchantment(1, 8, [TYPES.Helmet, TYPES.Chestplate, TYPES.Leggings, TYPES.Boots, TYPES.Elytra]),
  "Curse of Vanishing":    Enchantment(1, 8, [TYPES.Helmet, TYPES.Chestplate, TYPES.Leggings, TYPES.Boots, TYPES.Sword, TYPES.Axe, TYPES.Pickaxe, TYPES.Shovel, TYPES.Hoe, TYPES.Shears, TYPES.Bow, TYPES.Crossbow, TYPES.FishingRod, TYPES.FlintAndSteel, TYPES.Trident, TYPES.Shield, TYPES.Elytra]),
  "Impaling":              Enchantment(5, 4, [TYPES.Trident]),
  "Riptide":               Enchantment(3, 4, [TYPES.Trident], ['Loyalty', 'Channeling']),
  "Loyalty":               Enchantment(3, 1, [TYPES.Trident], ['Riptide']),
  "Channeling":            Enchantment(1, 8, [TYPES.Trident], ['Riptide']),
  "Multishot":             Enchantment(1, 4, [TYPES.Crossbow], ['Piercing']),
  "Piercing":              Enchantment(4, 1, [TYPES.Crossbow], ['Multishot']),
  "Quick Charge":          Enchantment(3, 2, [TYPES.Crossbow]),
  "Soul Speed":            Enchantment(3, 8, [TYPES.Boots]),
  "Sweeping Edge":         Enchantment(3, 4, [TYPES.Sword]),
}

const get_enchantment_name = (name, info) => {
  return ENCHANTMENTS[name].level === 1 ? name : `${name} ${ROMAN_NUMERALS[info.level - 1]}`
}

const xp = [0,7,16,27,40,55,72,91,112,135,160,187,216,247,280,315,352,394,441,493,550,612,679,751,828,910,997,1089,1186,1288,1395,1507,1628,1758,1897,2045,2202,2368,2543,2727,2920]

const Item = (type, penalty = 0, enchantments = {}) => {
  const item = {
    type,
    penalty,
    levels: 0,
    cost: 0,
    xp: 0,
    enchantments,
  }
  item.add_enchantment = (name, info) => {
    item.enchantments[name] = info
  }
  item.get_penalty = () => {
    return (2 ** item.penalty) - 1
  }
  item.is_better = (o, field) => {
    const a = field === COST.XP ? [item.xp, item.levels, item.penalty] : [item.levels, item.xp, item.penalty]
    const b = field === COST.XP ? [o.xp, o.levels, o.penalty] : [o.levels, o.xp, o.penalty]

    for (var i = 0; i < a.length; i++) {
      if (a[i] < b[i]) return true;
      if (a[i] > b[i]) return false;
    }
    return false;
  }
  item.hash = (m = 1) => {
    const he = Object.keys(item.enchantments).sort().map(name => item.enchantments[name].hash(name)).join()
    return `${item.type};${item.penalty * m};{${he}}`
  }
  return item
}

const combine = (_target, sacrifice) => {
  if (_target.type !== sacrifice.type && sacrifice.type !== TYPES.Book)
    return undefined

  var target = Item(_target.type, Math.max(_target.penalty, sacrifice.penalty) + 1)
  Object.keys(_target.enchantments).forEach(name => {
    target.enchantments[name] = Enchantment(_target.enchantments[name].level)
  })

  var cost = _target.get_penalty() + sacrifice.get_penalty()
  Object.keys(sacrifice.enchantments).forEach(name => {
    const [can_apply, penalty, reason] = ENCHANTMENTS[name].can_apply(target)
    if (!can_apply) {
      cost += penalty
      return
    }
    if (name in target.enchantments && target.enchantments[name].level === sacrifice.enchantments[name].level && ENCHANTMENTS[name].level > sacrifice.enchantments[name].level) {
      target.enchantments[name].level += 1
    } else {
      var result = {}
      Object.assign(result, sacrifice.enchantments[name])
      target.enchantments[name] = result
    }
    cost += target.enchantments[name].level * ENCHANTMENTS[name].get_multiplier(sacrifice.type)
  })
  if (cost > 39) {
    return undefined
  }
  target.cost = cost
  target.xp = xp[cost] + _target.xp + sacrifice.xp
  target.levels = cost + _target.levels + sacrifice.levels
  return target
}

function solve(target, items, field = COST.XP) {
  const th = target.hash(0)

  const size = 2 ** MAX_ITEMS - 1
  const masks = Array.from({length: size + 1}, () => ({}))
  items.forEach((item, index) => {
    masks[2 ** index][item.hash()] = {
      result: item,
      p1: {
        hash: undefined,
        mask: undefined
      },
      p2: {
        hash: undefined,
        mask: undefined
      },
    }
  })
  var best = undefined
  masks.forEach((results, index) => {
    for (var s = (size - index); s; s = (s - 1) & (size - index)) {
      const nmask = (s | index);
      Object.values(results).forEach(item1 => {
        Object.values(masks[s]).forEach(item2 => {
          const add = (t1, t2, m1, m2) => {
            const t = combine(t1, t2)
            if (t === undefined) return;
            const h = t.hash()
            if ((h in masks[nmask] && t.is_better(masks[nmask][h].result, field)) || !(h in masks[nmask])) {
              masks[nmask][h] = {
                result: t,
                p1: {
                  hash: t1.hash(),
                  mask: m1,
                },
                p2: {
                  hash: t2.hash(),
                  mask: m2,
                }
              }
            }
            if (t.hash(0) === th) {
              if (best === undefined || t.is_better(best.result, field)) {
                best = masks[nmask][h]
              }
            }
          }

          add(item1.result, item2.result, index, s)
          add(item2.result, item1.result, s, index)
        })
      })
    }
  })

  const steps = []

  var queue = [best]
  while (queue.length) {
    const last = queue.pop()
    if (last === undefined) continue;

    if (last.p1.mask === undefined) continue;

    const p1 = masks[last.p1.mask][last.p1.hash]
    const p2 = masks[last.p2.mask][last.p2.hash]

    steps.push([p1.result, p2.result, last.result])

    queue.push(p1)
    queue.push(p2)
  }

  return steps
}

export {
  ENCHANTMENTS,
  TYPES,
  MAX_PENALTY,
  MAX_ITEMS,
  COST,
  Item,
  Enchantment,
  solve,
  get_enchantment_name,
}
