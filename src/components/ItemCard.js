import React from 'react'

import ItemIcon from '../components/icons/ItemIcon'
import CloseIcon from '../components/icons/CloseIcon'

import { get_enchantment_name } from '../enchanter'

const ItemCard = ({ item, onEdit, onRemove, color }) => {
  return (
    <div className="card" onClick={onEdit} style={{backgroundColor: color}}>
      <ItemIcon type={item.type} className="card-photo" />
      <div className="enchantment-list">
        {
          Object.entries(item.enchantments).map(([name, info]) => (
            <p key={name}>{get_enchantment_name(name, info)}</p>
          ))
        }
      </div>
      <div className="card-buttons">
        {
          onRemove === undefined
          ? null
          :
          <div className="svg-button"
            onClick={(e) => {
              e.stopPropagation(); onRemove()}
            }
          >
            <CloseIcon />
          </div>
        }
      </div>
    </div>
  )
}

export default ItemCard
