import React from 'react'

import { TYPES } from '../../enchanter'

import Book from '../../images/Book.png'
import Helmet from '../../images/Helmet.png'
import Chestplate from '../../images/Chestplate.png'
import Leggings from '../../images/Leggings.png'
import Boots from '../../images/Boots.png'
import Sword from '../../images/Sword.png'
import Axe from '../../images/Axe.png'
import Pickaxe from '../../images/Pickaxe.png'
import Shovel from '../../images/Shovel.png'
import Hoe from '../../images/Hoe.png'
import Shears from '../../images/Shears.png'
import Bow from '../../images/Bow.png'
import Crossbow from '../../images/Crossbow.png'
import FishingRod from '../../images/FishingRod.png'
import FlintAndSteel from '../../images/FlintAndSteel.png'
import Trident from '../../images/Trident.png'
import Shield from '../../images/Shield.png'
import Elytra from '../../images/Elytra.png'

const IMAGES = {
  [TYPES.Book]: Book,
  [TYPES.Helmet]: Helmet,
  [TYPES.Chestplate]: Chestplate,
  [TYPES.Leggings]: Leggings,
  [TYPES.Boots]: Boots,
  [TYPES.Sword]: Sword,
  [TYPES.Axe]: Axe,
  [TYPES.Pickaxe]: Pickaxe,
  [TYPES.Shovel]: Shovel,
  [TYPES.Hoe]: Hoe,
  [TYPES.Shears]: Shears,
  [TYPES.Bow]: Bow,
  [TYPES.Crossbow]: Crossbow,
  [TYPES.FishingRod]: FishingRod,
  [TYPES.FlintAndSteel]: FlintAndSteel,
  [TYPES.Trident]: Trident,
  [TYPES.Shield]: Shield,
  [TYPES.Elytra]: Elytra,
}

const ItemIcon = ({ type, className }) => (
  <img src={IMAGES[type]} className={className} alt={type} />
)

export default ItemIcon
