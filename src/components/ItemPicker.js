import React, { useState, useEffect } from 'react'
import Modal from 'react-modal'

import ItemIcon from '../components/icons/ItemIcon'

import { ENCHANTMENTS, TYPES, MAX_PENALTY, Item, Enchantment, get_enchantment_name } from '../enchanter'

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    width: '300px',
    transform: 'translate(-50%, -50%)',
    display: 'flex',
    flexDirection: 'column',
  },
}

const EnchantmentPicker = ({type, name, info, change, remove}) => {
  const [value, setValue] = useState(name)
  const [availableEnchantments, setAvailableEnchantments] = useState([])
  const [enchantments, setEnchantments] = useState([])

  useEffect(() => {
    const all_enchantments_names = []
    const all_enchantments = {}
    const item = Item(type)
    var has_set_value = false
    Object.keys(ENCHANTMENTS).forEach(ename => {
      if (!ENCHANTMENTS[ename].can_apply(item)[0]) {
        return
      }
      for (var i = 0; i < ENCHANTMENTS[ename].level; i++) {
        const e = [ename, Enchantment(i + 1)]
        const str = get_enchantment_name(e[0], e[1])
        all_enchantments_names.push(str)
        all_enchantments[str] = e

        if (value === undefined && !has_set_value) {
          change(all_enchantments[str])
          has_set_value = true
        }
        if (name === ename && info.level === i + 1 && !has_set_value) {
          setValue(str)
          has_set_value = true
        }
      }
    })
    setAvailableEnchantments(all_enchantments_names)
    setEnchantments(all_enchantments)
  }, [type, name, info, change, value])

  return (
    <div className="enchantment-picker">
      <select value={value} onChange={(e) => {
        setValue(e.target.value)
        change(enchantments[e.target.value])
      }}>
      {
        availableEnchantments.map(name => (
          <option value={name} key={name}>{name}</option>
        ))
      }
      </select>
      <button onClick={remove}>
        Remove
      </button>
    </div>
  )
}

const ItemPicker = ({item, showAnvilUsesPicker, targetType, onDone, isOpen}) => {
  const [type, setType] = useState('Book')
  const [penalty, setPenalty] = useState(0)
  const [enchantments, setEnchantments] = useState([])

  useEffect(() => {
    setType(item?.type)
    setPenalty(item?.penalty)
    setEnchantments(Object.entries(item?.enchantments ?? {}))
  }, [item])

  return (
    <Modal
      isOpen={isOpen}
      style={customStyles}
    >
      <div className="item-general-information">
        <ItemIcon type={type} className="modal-photo" />
        <div>
          Item Type: <select className="type-selector" value={type} onChange={(e) => {
            const newType = e.target.value
            const newItem = Item(newType)
            const cp = []
            enchantments.forEach(([name, info]) => {
              if (ENCHANTMENTS[name].can_apply(newItem)[0]) {
                cp.push([name, info])
              }
            })
            if (cp.length !== enchantments.length) {
              if (!window.confirm(`Changing the type to ${newType} will remove ${enchantments.length - cp.length} incompatible enchantment(s)`)) {
                return
              }
              setEnchantments(cp)
            }
            setType(newType)
          }}>
            {
              Object.values(TYPES).map(name => {
                return targetType === undefined || [TYPES.Book, targetType].includes(name)
                  ? <option value={name} key={name}>{name}</option>
                  : null
              })
            }
          </select>
          {
            showAnvilUsesPicker
              ? <>Anvil Uses:
                  <select className="type-selector" value={penalty} onChange={e => setPenalty(e.target.value)}>
                    {
                      new Array(MAX_PENALTY + 1).fill(0).map((_, index) => (
                        <option value={index} key={index}>{index}</option>
                      ))
                    }
                  </select>
                </>
              : null
          }
          </div>
      </div>
      {
        enchantments.map(([name, info], index) => (
          <EnchantmentPicker
            type={type}
            name={name}
            info={info}
            change={(v) => {
              const cp = [...enchantments]
              cp[index] = v
              setEnchantments(cp)
            }}
            remove={() => {
              const cp = [...enchantments]
              cp.splice(index, 1)
              setEnchantments(cp)
            }}
            key={`${name};${info?.level}`}/>
        ))
      }
      <button
        onClick={() => { setEnchantments([...enchantments, [undefined, undefined]]) }}
        className="add-enchantment-button"
      >
        Add Enchantment
      </button>
      <button
        onClick={() => {
          const newItem = Item(type, penalty)
          var is_valid_item = true
          enchantments.forEach(([name, info]) => {
            if (!is_valid_item) return

            if (name in newItem.enchantments) {
              is_valid_item = false
              window.alert(`${name} is already present in the item`)
              return
            }

            const [can_apply, cost, reason] = ENCHANTMENTS[name].can_apply(newItem)
            if (!can_apply) {
              is_valid_item = false
              window.alert(`${get_enchantment_name(name, info)} is not compatible with ${reason}`)
              return
            }
            newItem.add_enchantment(name, info)
          })
          if (is_valid_item) {
            onDone(newItem)
          }
        }}
        className="modal-done-button"
      >
        Done
      </button>
    </Modal>
  )
}

export default ItemPicker
